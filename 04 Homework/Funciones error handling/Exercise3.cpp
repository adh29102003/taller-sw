/**
 * @file Exercise3.cpp
 * @author Alexis D�az Huaringa (alexis.diaz.h@uni.pe)
 * @brief exercise 3
 * @version 1.0
 * @date 24.02.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include "math.h"
#include <windows.h>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float firstNumberInput=0.0;
float secondNumberInput=0.0;
float thirdNumberInput=0.0;
float maxNumber;
float midNumber;
float minNumber;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
bool CollectData();
bool Calculate();
bool ShowResults();
float getMaxNumber(float number1,float number2);
float getMinNumber(float number1,float number2);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
*  												FUNCTION DEFINITION
*******************************************************************************************************************************************/

bool Run(){
	while (true){
		if (CollectData() ==false){
			cout<<"Error en CollectData\r\n";
			continue;
		}
		if(Calculate()==false){
			cout<<"Error en Calculate\r\n";
			continue;
		}
		if(ShowResults()==false){
			cout<<"Error en ShowResults\r\n";
			continue;
		}
		break;
	}
	return true;
}
//=====================================================================================================

bool CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Insert the first number: ";
	cin>>firstNumberInput;
	if(cin.fail()){
		cin.clear();
		cin.ignore(1000,'\n');
		firstNumberInput=0;
		cout<<"The first number typed is not valid"<<endl; 
		return false; 
	}
	cout<<"Insert the second number: ";
	cin>>secondNumberInput;
	if(cin.fail()){
		cin.clear();
		cin.ignore(1000,'\n');
		secondNumberInput=0;
		cout<<"The second number typed is not valid"<<endl; 
		return false; 
	}
	cout<<"Insert the third number: ";
	cin>>thirdNumberInput;
	if(cin.fail()){
		cin.clear();
		cin.ignore(1000,'\n');
		thirdNumberInput=0;
		cout<<"The third number typed is not valid"<<endl; 
		return false; 
	}
	return true;
}
//=====================================================================================================

bool Calculate(){
	float a;
	float b;
	a=getMaxNumber(firstNumberInput,secondNumberInput);
	b=getMinNumber(firstNumberInput,secondNumberInput);
	firstNumberInput=a;
	secondNumberInput=b;
	a=getMaxNumber(secondNumberInput,thirdNumberInput);
	b=getMinNumber(secondNumberInput,thirdNumberInput);
	secondNumberInput=a;
	thirdNumberInput=b;
	a=getMaxNumber(firstNumberInput,secondNumberInput);
	b=getMinNumber(firstNumberInput,secondNumberInput);
	firstNumberInput=a;
	secondNumberInput=b;
	a=getMaxNumber(secondNumberInput,thirdNumberInput);
	b=getMinNumber(secondNumberInput,thirdNumberInput);
	secondNumberInput=a;
	thirdNumberInput=b;
	return true;
}
//=====================================================================================================

bool ShowResults(){
	cout<<"\r\n============Show result============\r\n";
	cout<<"The order of the numbers is: ";
	cout<<"\n\t"<<firstNumberInput<<"   "<<secondNumberInput<<"   "<<thirdNumberInput;
	return true;
}
//=====================================================================================================

float getMaxNumber(float number1,float number2){
	float max=number1;
	if (number1<number2)
		max=number2;
	return max;
}
//=====================================================================================================

float getMinNumber(float number1,float number2){
	float min=number1;
	if (number1>number2)
		min=number2;
	return min;
}

//=====================================================================================================
