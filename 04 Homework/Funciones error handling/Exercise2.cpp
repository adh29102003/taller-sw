/**
 * @file Exercise2.cpp
 * @author Alexis D�az Huaringa (alexis.diaz.h@uni.pe)
 * @brief exercise 2
 * @version 1.0
 * @date 24.02.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include "math.h"
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float salaryInput=0.0;
float additionalSalary=0.0;
double discountFactor=0.0;
double discountCalculate=0.0;
double netSalary=0;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
bool CollectData();
bool Calculate();
bool ShowResults();
double CalculateDiscountFactor(float salary);
double CalculateNetSalary(float salary, double discount);
double CalculateDiscount(double discountFctor);
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
*  												FUNCTION DEFINITION
*******************************************************************************************************************************************/

bool Run(){
	while (true){
		if (CollectData() ==false){
			cout<<"Error en CollectData\r\n";
			continue;
		}
		if(Calculate()==false){
			cout<<"Error en Calculate\r\n";
			continue;
		}
		if(ShowResults()==false){
			cout<<"Error en ShowResults\r\n";
			continue;
		}
		break;
	}
	return true;
}
//=====================================================================================================

bool CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Insert starting salary: ";
	cin>>salaryInput;
	if (cin.fail()){
		cin.clear();
		cin.ignore(1000,'\n');
		salaryInput=0;
		cout<<"Typed salary is not valid"<<endl;
		return false;
	}
	return true;
}
//=====================================================================================================

bool Calculate(){
	if (salaryInput<0){
		cout<<"Type salary positive"<<endl;
		return false;
	}else{
		discountFactor=CalculateDiscountFactor(salaryInput);
		discountCalculate=CalculateDiscount(discountFactor);
		netSalary=CalculateNetSalary(salaryInput,discountCalculate);
	}
	return true;
}
//=====================================================================================================

bool ShowResults(){
	cout<<"\r\n============Show result============\r\n";
	cout<<"The discount is: ";
	cout<<discountCalculate<<endl;
	cout<<"The net salary is: ";
	cout<<"\n\t"<<netSalary;
	return true;
}
//=====================================================================================================

double CalculateDiscountFactor(float salary){
	float Factor=0.0;
	if (salary<=1000){
		Factor=0.1;
		additionalSalary=0;
	}else if (salary<=2000){
		Factor=0.05;
		additionalSalary=salary-1000;
	}else{
		Factor=0.03;
		additionalSalary=salary-2000;
	}
	return Factor;	
}

//=====================================================================================================

double CalculateNetSalary(float salary, double discountC){
	return salary-discountC;
}
//=====================================================================================================

double CalculateDiscount(double discountFctor){
	return (discountFctor*additionalSalary);
}
