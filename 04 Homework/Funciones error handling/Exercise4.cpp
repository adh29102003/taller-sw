/**
 * @file Exercise4.cpp
 * @author Alexis D�az Huaringa (alexis.diaz.h@uni.pe)
 * @brief exercise 4
 * @version 1.0
 * @date 24.02.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <windows.h>
#include "math.h"
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float numberMinutesInput=0.0;
int days=0.0;
int hours=0.0;
float minutes=0.0;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
bool CollectData();
bool Calculate();
bool ShowResults();
double CalculateDays(float minutes);
double CalculateHours(float minutes);
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
*  												FUNCTION DEFINITION
*******************************************************************************************************************************************/

bool Run(){
	while (true){
		if (CollectData() ==false){
			cout<<"Error en CollectData\r\n";
			continue;
		}
		if(Calculate()==false){
			cout<<"Error en Calculate\r\n";
			continue;
		}
		if(ShowResults()==false){
			cout<<"Error en ShowResults\r\n";
			continue;
		}
		break;
	}
	return true;
}
//=====================================================================================================

bool CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Insert the number of minutes: ";
	cin>>numberMinutesInput;
	if(cin.fail()){
		cin.clear();
		cin.ignore(1000,'\n');
		numberMinutesInput=0;
		cout<<"The number of minutes typed is not valid"<<endl; 
		return false; 
	}
	return true;
}
//=====================================================================================================

bool Calculate(){
	if (numberMinutesInput<0){
		cout<<"Error. The number of minutes is negative"<<endl;
		return false;
	}else{
		days=CalculateDays(numberMinutesInput);
		hours=CalculateHours(numberMinutesInput);
		minutes=numberMinutesInput;
	}
	return true; 
}
//=====================================================================================================

bool ShowResults(){
	cout<<"\r\n============Show result============\r\n";
	cout<<"The number of minutes is: ";
	cout<<"\n\t"<<days<<" days "<<hours<<" hours " <<minutes<<" minutes";
	return true;
}
//=====================================================================================================

double CalculateDays(float minutes){
	int day=0;
	if (numberMinutesInput>1440){
		day=numberMinutesInput/1440;
		numberMinutesInput-=1440*day;
	}
	return day;
	
}
double CalculateHours(float minutes){
	int hour=0;
	if (numberMinutesInput>60){
		hour=numberMinutesInput/60;
		numberMinutesInput-=60*hour;
	}
	return hour;
}
//=====================================================================================================

