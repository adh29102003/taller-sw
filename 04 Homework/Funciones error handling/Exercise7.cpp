/**
 * @file Exercise7.cpp
 * @author Alexis D�az Huaringa (alexis.diaz.h@uni.pe)
 * @brief exercise 7
 * @version 1.0
 * @date 24.02.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include "math.h"
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int distanceTraveledInput=0;
int fixedAmount=250;
int distanceA=300;
int distanceB=1000;
int firstFee=30;
int secondFee=20;
int payFeeDistance=0;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
bool CollectData();
bool Calculate();
bool ShowResults();
int CalculatePayFeeDistance(int distanceTraveledInput,int firstFee, int secondFee);
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
*  												FUNCTION DEFINITION
*******************************************************************************************************************************************/

bool Run(){
	while (true){
		if (CollectData() ==false){
			cout<<"Error in CollectData\r\n";
			continue;
		}
		if(Calculate()==false){
			cout<<"Error in Calculate\r\n";
			continue;
		}
		if(ShowResults()==false){
			cout<<"Error in ShowResults\r\n";
			continue;
		}
		break;
	}
	return true;
}
//=====================================================================================================

bool CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Insert the travel distance: ";
	cin>>distanceTraveledInput;
	if(cin.fail()){
		cin.clear();
		cin.ignore(1000,'\n');
		distanceTraveledInput=0;
		cout<<"The distance typed is not valid"<<endl;
		return false;
	}
	return true; 
}
//=====================================================================================================

bool Calculate(){
	if(distanceTraveledInput<0){
		cout<<"Insert positive value"<<endl;
		return false;
	}else{
		payFeeDistance+=fixedAmount;
		payFeeDistance=CalculatePayFeeDistance(distanceTraveledInput,firstFee,secondFee);
	}
	return true;
}
//=====================================================================================================

bool ShowResults(){
	cout<<"\r\n============Show result============\r\n";
	cout<<"The rent payment is: ";
	cout<<"\n\t"<<payFeeDistance;
	return true;
}
//=====================================================================================================

int CalculatePayFeeDistance(int distanceTraveledInput,int firstFee, int secondFee){
	if (distanceTraveledInput>distanceB){
		payFeeDistance+=secondFee*(distanceTraveledInput-distanceB)+(distanceB-distanceA)*firstFee;
	}else if (distanceTraveledInput>distanceA){
		payFeeDistance+=firstFee*(distanceTraveledInput-distanceA);
	}else{
		payFeeDistance+=0;
	}
	return payFeeDistance;
}
//=====================================================================================================
