/**
 * @file Exercise9.cpp
 * @author Alexis D�az Huaringa (alexis.diaz.h@uni.pe)
 * @brief exercise 9
 * @version 1.0
 * @date 24.02.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include "math.h"
#include <windows.h>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float totalSales=0.0;
float firstFactorCommission=0.1;
float secondFactorCommission=0.09;
float secondBonusCommission=50;
float totalCommission=0.0;
int numberCommission=0;


/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
bool CollectData();
bool Calculate();
bool ShowResults();
int GetNumberCommmission(float sales);
float CalculateTotalComission(int numberCode, float sales);
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
*  												FUNCTION DEFINITION
*******************************************************************************************************************************************/

bool Run(){
	while (true){
		if (CollectData() ==false){
			cout<<"Error in CollectData\r\n";
			continue;
		}
		if(Calculate()==false){
			cout<<"Error in Calculate\r\n";
			continue;
		}
		if(ShowResults()==false){
			cout<<"Error in ShowResults\r\n";
			continue;
		}
		break;
	}
	return true;
}

//=====================================================================================================

bool CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Insert total sales: ";
	cin>>totalSales;
	if (cin.fail()){
		cin.clear();
		cin.ignore(1000,'\n');
		totalSales=0;
		cout<<"The sales typed is not valid"<<endl;
		return false;
	}
	return true;
}
//=====================================================================================================

bool Calculate(){
	if(totalSales<0){
		cout<<"Insert positive value"<<endl;
		return false;
	}else{
		numberCommission=GetNumberCommmission(totalSales);
		totalCommission=CalculateTotalComission(numberCommission,totalSales);
	}
	return true;
}
//=====================================================================================================

bool ShowResults(){
	cout<<"\r\n============Show result============\r\n";
	cout<<"The total commission is: "<<totalCommission;
	return true;
}
//=====================================================================================================

int GetNumberCommmission(float sales){
	int number;
	if (sales<=150){
		number=0;
	}else if(sales>150 && sales<=400){
		number=1;
	}else{
		number=2;
	}
	return number;
}

//=====================================================================================================

float CalculateTotalComission(int numberCode, float sales){
	float commission;
	switch (numberCode){
		case 0:
			commission=0;
		break;
		case 1:
			commission=firstFactorCommission*sales;
		break;
		case 2:
			commission=secondFactorCommission*sales+secondBonusCommission;
		break;
			
	}
	return commission;
}

//=====================================================================================================
