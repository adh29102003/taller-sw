/**
 * @file Exercise5.cpp
 * @author Alexis D�az Huaringa (alexis.diaz.h@uni.pe)
 * @brief exercise 5
 * @version 1.0
 * @date 24.02.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <windows.h>
#include "math.h"
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float aSideTriangle=0.0;
float bSideTriangle=0.0;
float cSideTriangle=0.0;
bool existenceTriangle;
float perimeterTriangle=0;
float areaTriangulo=0;
string typeTriangle;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
bool CollectData();
bool Calculate();
bool ShowResults();
bool CheckExistenceTriangle(float sideA,float sideB, float sideC);
double CalculatePerimeterTriangle(float sideA,float sideB, float sideC);
double CalculteAreaTriangle(float sideA, float sideB, float sideC, float perimeter);
string TypeTriangle(float sideA, float sideB, float sideC);
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
*  												FUNCTION DEFINITION
*******************************************************************************************************************************************/

bool Run(){
	while (true){
		if (CollectData() ==false){
			cout<<"Error en CollectData\r\n";
			continue;
		}
		if(Calculate()==false){
			cout<<"Error en Calculate\r\n";
			continue;
		}
		if(ShowResults()==false){
			cout<<"Error en ShowResults\r\n";
			continue;
		}
		break;
	}
	return true;
}
//=====================================================================================================

bool CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Insert side 'A': ";
	cin>>aSideTriangle;
	if (cin.fail()){
		cin.clear();
		cin.ignore(1000,'\n');
		aSideTriangle=0;
		cout<<"Error in the side a"<<endl;
		return false;
	}
	cout<<"Insert side 'B': ";
	cin>>bSideTriangle;
	if (cin.fail()){
		cin.clear();
		cin.ignore(1000,'\n');
		bSideTriangle=0;
		cout<<"Error in the side b"<<endl;
		return false;
	}
	cout<<"Insert side 'C': ";
	cin>>cSideTriangle;
	if (cin.fail()){
		cin.clear();
		cin.ignore(1000,'\n');
		cSideTriangle=0;
		cout<<"Error in the side c"<<endl;
		return false;
	}
	return true;
}
//=====================================================================================================

bool Calculate(){
	if((aSideTriangle<0 )||(bSideTriangle<0)||(cSideTriangle<0)){
		cout<<"Insert values positive"<<endl;
		return false;
	}else{
	existenceTriangle=CheckExistenceTriangle(aSideTriangle,bSideTriangle,cSideTriangle);
	perimeterTriangle=CalculatePerimeterTriangle(aSideTriangle,bSideTriangle,cSideTriangle);
	areaTriangulo=CalculteAreaTriangle(aSideTriangle,bSideTriangle,cSideTriangle,perimeterTriangle);
	typeTriangle=TypeTriangle(aSideTriangle,bSideTriangle,cSideTriangle);
	}
	return true;
}
//=====================================================================================================

bool ShowResults(){
	cout<<"\r\n============Show result============\r\n";
	if (existenceTriangle){
		cout<<"The type is: "<<typeTriangle<<endl;
		cout<<"Its area is: "<<areaTriangulo<<"  and the perimeter is: "<<perimeterTriangle<<endl;
	}else{
		cout<<"No existence"<<endl;
		return false;
	}
	return true;
}
//=====================================================================================================

bool CheckExistenceTriangle(float sideA,float sideB, float sideC){
	bool existence=false;
	float difside=sideA-sideB;
	if (difside<0)
		difside*=-1;
	if ((sideA+sideB)>sideC && difside<sideC)
		existence=true;
	return existence; 
}
double CalculatePerimeterTriangle(float sideA,float sideB, float sideC){
	return sideA+sideB+sideC;
}
double CalculteAreaTriangle(float sideA, float sideB, float sideC, float perimeter){
	float p=perimeter/2.0;
	return sqrt(p*(p-sideA)*(p-sideB)*(p-sideC));
}
//=====================================================================================================
string TypeTriangle(float sideA, float sideB, float sideC){
	string triangle;
	int accountant=0;
	if (sideA==sideB)
		accountant+=1;
	if (sideA==sideC)
		accountant+=1;
	if (sideB==sideC)
		accountant+=1;
	if (accountant==1){
		triangle="sosceles";
	}else{

		if (accountant==3){
			triangle="Equilateral";
		}else{
			triangle="Scalene";
		}
	}
	return triangle;
}
