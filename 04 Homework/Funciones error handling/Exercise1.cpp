/**
 * @file Exercise1.cpp
 * @author Alexis D�az Huaringa (alexis.diaz.h@uni.pe)
 * @brief exercise 1
 * @version 1.0
 * @date 29.01.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include<windows.h>
#include "math.h"
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int hoursMax=40;
float salaryMax=500.00;
float workHoursInput=0.0;
float payHoursInput=0.0;
float payForHoursExtraFactor=1.5;
bool hasBonus;
bool hasdiscount;
double grossSalary=0;
double payHoursBonus=0;
double discountPay=0;
double payTotalWeek=0;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
bool CollectData();
bool Calculate();
bool ShowResults();
double CalculateGrossSalary(float workHours, float payHours);
double CalculatePayBonus(float workHours, float payHour, int hoursMax);
double CalculateDiscount(double grossSalary);
bool CheckHoursWork(float workHours);
bool CheckGrossSalary(double grossSalary);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
*  												FUNCTION DEFINITION
*******************************************************************************************************************************************/

void Run(){
	while (true){
		if (CollectData() ==false){
			cout<<"Error en CollectData\r\n";
			continue;
		}
		if(Calculate()==false){
			cout<<"Error en Calculate\r\n";
			continue;
		}
		if(ShowResults()==false){
			cout<<"Error en ShowResults\r\n";
			continue;
		}
		break;
	}
}
//=====================================================================================================

bool CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Insert total hours: ";
	cin>>workHoursInput;
	if (cin.fail()==true){
		cin.clear();
		cin.ignore(1000,'\n');
		workHoursInput = 0;
		cout<<"Typed hours work is not valid\r\n";
		return false;
	}
	cout<<"Insert pay for hour work: ";
	cin>>payHoursInput;
	if (cin.fail()==true){
		cin.clear();
		cin.ignore(1000,'\n');
		 payHoursInput= 0;
		cout<<"Typed pay for hour no valid\r\n";
		return false;
	}
	return true;
}
//=====================================================================================================

bool Calculate(){
	if( workHoursInput<0 || payHoursInput<0){
		cout<<"Insert values positives"<<endl;
		return false;
	}else{
		hasBonus=CheckHoursWork(workHoursInput);
		if (hasBonus){
			payHoursBonus=CalculatePayBonus(workHoursInput, payHoursInput, hoursMax);
			grossSalary=hoursMax*payHoursInput;
			grossSalary+=payHoursBonus;
		}
		else{
			grossSalary=CalculateGrossSalary(workHoursInput, payHoursInput);}
		hasdiscount=CheckGrossSalary(grossSalary);
		if (hasdiscount)
			discountPay=CalculateDiscount(grossSalary);
		payTotalWeek=grossSalary-discountPay;
	}
	return true;
}
//=====================================================================================================

bool ShowResults(){
	if (payTotalWeek<0){
		return false;
	}else{
		cout<<"\r\n============Show result============\r\n";
		cout<<"The week pay is: ";
		cout<<"\n\t"<<payTotalWeek;
	}
	return true;
}

//=====================================================================================================
double CalculateGrossSalary(float workHours, float payHours){
	return workHours*payHours; 
}
//=====================================================================================================

double CalculatePayBonus(float workHours, float payHour, int hoursMax){
	return (workHours-hoursMax)*payHour*payForHoursExtraFactor; 
}
//=====================================================================================================

bool CheckHoursWork(float workHours){
	bool bonus=false;
	if (workHours>hoursMax){
		bonus=true;
	}
	return bonus;
}
//=====================================================================================================

bool CheckGrossSalary(double salary){
	bool discount=false;
	if (salary>salaryMax){
		discount=true;
	}
	return discount;
}
//=====================================================================================================
double CalculateDiscount(double grossSalary){
	return grossSalary*0.1;
}
