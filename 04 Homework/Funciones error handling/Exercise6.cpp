/**
 * @file Exercise6.cpp
 * @author Alexis D�az Huaringa (alexis.diaz.h@uni.pe)
 * @brief exercise 6
 * @version 1.0
 * @date 24.02.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include "math.h"
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int hoursInput=0;
int minutesInput=0;
int secondsInput=0;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
bool CollectData();
bool Calculate();
bool ShowResults();
void newTime();
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
*  												FUNCTION DEFINITION
*******************************************************************************************************************************************/

bool Run(){
	while (true){
		if (CollectData() ==false){
			cout<<"Error in CollectData\r\n";
			continue;
		}
		if(Calculate()==false){
			cout<<"Error in Calculate\r\n";
			continue;
		}
		if(ShowResults()==false){
			cout<<"Error in ShowResults\r\n";
			continue;
		}
		break;
	}
	return true;
}
//=====================================================================================================

bool CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Insert the time:";
	cout<<"\n\t Hours: ";
	cin>>hoursInput;
	if (cin.fail()){
		cin.clear();
		cin.ignore(1000,'\n');
		hoursInput=0;
		cout<<"The hours typed is not valid"<<endl;
		return false;
	}
	cout<<"\n\t Minutes: ";
	cin>>minutesInput;
	if (cin.fail()){
		cin.clear();
		cin.ignore(1000,'\n');
		minutesInput=0;
		cout<<"The minutes typed is not valid"<<endl;
		return false;
	}
	cout<<"\n\t Seconds: ";
	cin>>secondsInput;
	if (cin.fail()){
		cin.clear();
		cin.ignore(1000,'\n');
		secondsInput=0;
		cout<<"The seconds typed is not valid"<<endl;
		return false;
	}
	return true;
}
//=====================================================================================================

bool Calculate(){
	if((hoursInput<0) || (minutesInput<0)||(secondsInput<0)){
		cout<<"Insert positives values"<<endl;
		return false; 
	}else{
		newTime();
	}
	return true;
}
//=====================================================================================================

bool ShowResults(){
	if (hoursInput>24 || minutesInput>=60 || secondsInput>60){
		cout<<"Error in the time"<<endl;
		return false;
	}else{
		cout<<"\r\n============Show result============\r\n";
		cout<<"The time after that 1 seconds more: ";
		cout<<"\n\t"<<hoursInput<<" hours " <<minutesInput<<" minutes "<<secondsInput<<" seconds";
	}
	return true;
}
//=====================================================================================================

void newTime(){
	secondsInput+=1;
	if (secondsInput==60){
		minutesInput+=1;
		secondsInput=0;
		if (minutesInput==60){
			hoursInput+=1;
			minutesInput=0;
			if (hoursInput==24){
				hoursInput=0;
			}
		}
	}
}
//=====================================================================================================
