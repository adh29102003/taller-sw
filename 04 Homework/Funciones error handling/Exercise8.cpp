/**
 * @file Exercise8.cpp
 * @author Alexis D�az Huaringa (alexis.diaz.h@uni.pe)
 * @brief exercise 8
 * @version 1.0
 * @date 31.01.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <windows.h>
#include "math.h"
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int numberCodeEmployeeInput=0;
int ageEmployee=0;
string civilStatuEmployee;
string genderEmployee;


/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
bool CollectData();
bool Calculate();
bool ShowResults();
bool AskCode();
int GetAgeEmployee(int numberCode);
string GetCivilStatusEmployee(int numberCode);
string GetGenderEmployee(int numberCode);
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
*  												FUNCTION DEFINITION
*******************************************************************************************************************************************/

bool Run(){
	while (true){
		if (CollectData() ==false){
			cout<<"Error in CollectData\r\n";
			continue;
		}
		if(Calculate()==false){
			cout<<"Error in Calculate\r\n";
			continue;
		}
		if(ShowResults()==false){
			cout<<"Error in ShowResults\r\n";
			continue;
		}
		break;
	}
	return true;
}
//=====================================================================================================

bool CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Insert the employee code: ";
	if (AskCode()==false){
		return false; 
	}
	return true;
}
//=====================================================================================================

bool Calculate(){
	ageEmployee=GetAgeEmployee(numberCodeEmployeeInput);
	civilStatuEmployee=GetCivilStatusEmployee(numberCodeEmployeeInput);
	genderEmployee=GetGenderEmployee(numberCodeEmployeeInput);
	if (genderEmployee=="Gender no valid"){
		return false;
	}
	if(civilStatuEmployee=="Civil status no valid"){
		return false;
	}
	if(ageEmployee==0){
		return false;
	}
	return true; 
	
}
//=====================================================================================================

bool ShowResults(){
	if (Calculate()==true)
	{
		cout<<"\r\n============Show result============\r\n";
		cout<<"The civil status of employee is: ";
		cout<<"\n\t"<<civilStatuEmployee<<endl;
		cout<<"The age of employee is: ";
		cout<<"\n\t"<<ageEmployee<<endl;
		cout<<"The gender of employee is: ";
		cout<<"\n\t"<<genderEmployee<<endl;
		return true;
	}
	return false;
}
//=====================================================================================================

int GetAgeEmployee(int numberCode){
	int age;
	age=numberCode%1000;
	age=age/10;
	return age;
}
//=====================================================================================================

string GetCivilStatusEmployee(int numberCode){
	string status;
	int caseStatus=0;
	caseStatus=numberCode/1000;
	switch (caseStatus){
		case 1:
			status="Single";
			break;
		case 2:
			status="Married";
			break;
		case 3:
			status="Widowed";
			break;
		case 4:
			status="Divorced";
			break;
		default:
			cout<<"Civil status no valid"<<endl;
	}
	return status;
}
//=====================================================================================================

string GetGenderEmployee(int numberCode){
	string gender;
	int caseGender;
	caseGender=numberCode%10;
	if (caseGender==1){
		gender="Femenine";
	}else if(caseGender==2) {
		gender="Male";
		
	}else{
		gender="Gender no valid"<<endl;
	}
	return gender;
}
//=====================================================================================================
bool AskCode(){
	cin>>numberCodeEmployeeInput;
	if  (cin.fail()==true){
		cin.clear();
		cin.ignore(1000,'\n');
		numberCodeEmployeeInput=0;
		cout<<"Error in code typing"<<endl;
		return false;
	}
	if ((numberCodeEmployeeInput>=10000) || (numberCodeEmployeeInput<1000)){
		return false;
		cout<<"Error length is not valid \r\n";
	}
	return true;
}
