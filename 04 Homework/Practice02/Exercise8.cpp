/**
 * @file Exercise8.cpp
 * @author Alexis D�az Huaringa (alexis.diaz.h@uni.pe)
 * @brief exercise 8
 * @version 1.0
 * @date 31.01.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include "math.h"
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int numberCodeEmployeeInput=0;
int ageEmployee=0;
string civilStatuEmployee;
string genderEmployee;


/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
int GetAgeEmployee(int numberCode);
string GetCivilStatusEmployee(int numberCode);
string GetGenderEmployee(int numberCode);
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
*  												FUNCTION DEFINITION
*******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}
//=====================================================================================================

void CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Insert the employee code: ";
	cin>>numberCodeEmployeeInput;
}
//=====================================================================================================

void Calculate(){
	ageEmployee=GetAgeEmployee(numberCodeEmployeeInput);
	civilStatuEmployee=GetCivilStatusEmployee(numberCodeEmployeeInput);
	genderEmployee=GetGenderEmployee(numberCodeEmployeeInput);
	
}
//=====================================================================================================

void ShowResults(){
	cout<<"\r\n============Show result============\r\n";
	cout<<"The civil status of employee is: ";
	cout<<"\n\t"<<civilStatuEmployee<<endl;
	cout<<"The age of employee is: ";
	cout<<"\n\t"<<ageEmployee<<endl;
	cout<<"The gender of employee is: ";
	cout<<"\n\t"<<genderEmployee<<endl;
}
//=====================================================================================================

int GetAgeEmployee(int numberCode){
	int age;
	age=numberCode%1000;
	age=age/10;
	return age;
}
//=====================================================================================================

string GetCivilStatusEmployee(int numberCode){
	string status;
	int caseStatus=0;
	caseStatus=numberCode/1000;
	switch (caseStatus){
		case 1:
			status="Single";
			break;
		case 2:
			status="Married";
			break;
		case 3:
			status="Widowed";
			break;
		case 4:
			status="Divorced";
			break;
	}
	return status;
}
//=====================================================================================================

string GetGenderEmployee(int numberCode){
	string gender;
	int caseGender;
	caseGender=numberCode%10;
	if (caseGender==1){
		gender="Femenine";
	}else {
		if (caseGender==2){
			gender="Male";
		}
	}
	return gender;
}
//=====================================================================================================
