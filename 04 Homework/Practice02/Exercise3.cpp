/**
 * @file Exercise3.cpp
 * @author Alexis D�az Huaringa (alexis.diaz.h@uni.pe)
 * @brief exercise 3
 * @version 1.0
 * @date 30.01.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include "math.h"
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float firstNumberInput=0.0;
float secondNumberInput=0.0;
float thirdNumberInput=0.0;
float maxNumber;
float midNumber;
float minNumber;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
float getMaxNumber(float number1,float number2);
float getMinNumber(float number1,float number2);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
*  												FUNCTION DEFINITION
*******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}
//=====================================================================================================

void CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Insert the first number: ";
	cin>>firstNumberInput;
	cout<<"Insert the second number: ";
	cin>>secondNumberInput;
	cout<<"Insert the third number: ";
	cin>>thirdNumberInput;
}
//=====================================================================================================

void Calculate(){
	float a;
	float b;
	a=getMaxNumber(firstNumberInput,secondNumberInput);
	b=getMinNumber(firstNumberInput,secondNumberInput);
	firstNumberInput=a;
	secondNumberInput=b;
	a=getMaxNumber(secondNumberInput,thirdNumberInput);
	b=getMinNumber(secondNumberInput,thirdNumberInput);
	secondNumberInput=a;
	thirdNumberInput=b;
	a=getMaxNumber(firstNumberInput,secondNumberInput);
	b=getMinNumber(firstNumberInput,secondNumberInput);
	firstNumberInput=a;
	secondNumberInput=b;
	a=getMaxNumber(secondNumberInput,thirdNumberInput);
	b=getMinNumber(secondNumberInput,thirdNumberInput);
	secondNumberInput=a;
	thirdNumberInput=b;
}
//=====================================================================================================

void ShowResults(){
	cout<<"\r\n============Show result============\r\n";
	cout<<"The order of the numbers is: ";
	cout<<"\n\t"<<firstNumberInput<<"   "<<secondNumberInput<<"   "<<thirdNumberInput;
}
//=====================================================================================================

float getMaxNumber(float number1,float number2){
	float max=number1;
	if (number1<number2)
		max=number2;
	return max;
}
//=====================================================================================================

float getMinNumber(float number1,float number2){
	float min=number1;
	if (number1>number2)
		min=number2;
	return min;
}

//=====================================================================================================
