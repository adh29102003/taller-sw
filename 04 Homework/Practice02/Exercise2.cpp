/**
 * @file Exercise2.cpp
 * @author Alexis D�az Huaringa (alexis.diaz.h@uni.pe)
 * @brief exercise 2
 * @version 1.0
 * @date 24.02.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include "math.h"
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float salaryInput=0.0;
float additionalSalary=0.0;
double discountFactor=0.0;
double discountCalculate=0.0;
double netSalary=0;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
double CalculateDiscountFactor(float salary);
double CalculateNetSalary(float salary, double discount);
double CalculateDiscount(double discountFctor);
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
*  												FUNCTION DEFINITION
*******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}
//=====================================================================================================

void CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Insert starting salary: ";
	cin>>salaryInput;
}
//=====================================================================================================

void Calculate(){
	discountFactor=CalculateDiscountFactor(salaryInput);
	discountCalculate=CalculateDiscount(discountFactor);
	netSalary=CalculateNetSalary(salaryInput,discountCalculate);
}
//=====================================================================================================

void ShowResults(){
	cout<<"\r\n============Show result============\r\n";
	cout<<"The discount is: ";
	cout<<discountCalculate<<endl;
	cout<<"The net salary is: ";
	cout<<"\n\t"<<netSalary;
}
//=====================================================================================================

double CalculateDiscountFactor(float salary){
	float Factor=0.0;
	if (salary<=1000){
		Factor=0.1;
		additionalSalary=0;
		
	}else if (salary<=2000){
		Factor=0.05;
		additionalSalary=salary-1000;
	}else{
		Factor=0.03;
		additionalSalary=salary-2000;
	}
	return Factor;	
}

//=====================================================================================================

double CalculateNetSalary(float salary, double discountC){
	return salary-discountC;
}
//=====================================================================================================

double CalculateDiscount(double discountFctor){
	return (discountFctor*additionalSalary);
}
