/**
 * @file Exercise1.cpp
 * @author Alexis D�az Huaringa (alexis.diaz.h@uni.pe)
 * @brief exercise 1
 * @version 1.0
 * @date 29.01.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include "math.h"
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int hoursMax=40;
float salaryMax=500.00;
float workHoursInput=0.0;
float payHoursInput=0.0;
float payForHoursExtraFactor=1.5;
bool hasBonus;
bool hasdiscount;
double grossSalary=0;
double payHoursBonus=0;
double discountPay=0;
double payTotalWeek=0;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
double CalculateGrossSalary(float workHours, float payHours);
double CalculatePayBonus(float workHours, float payHour, int hoursMax);
double CalculateDiscount(double grossSalary);
bool CheckHoursWork(float workHours);
bool CheckGrossSalary(double grossSalary);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
*  												FUNCTION DEFINITION
*******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}
//=====================================================================================================

void CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Insert total hours: ";
	cin>>workHoursInput;
	cout<<"Insert pay for hour work: ";
	cin>>payHoursInput;
}
//=====================================================================================================

void Calculate(){
	grossSalary=CalculateGrossSalary(workHoursInput, payHoursInput);
	hasBonus=CheckHoursWork(workHoursInput);
	if (hasBonus)
		payHoursBonus=CalculatePayBonus(grossSalary, payHoursInput, hoursMax);
	hasdiscount=CheckGrossSalary(grossSalary);
	if (hasdiscount)
		discountPay=CalculateDiscount(grossSalary);
	payTotalWeek=grossSalary+payHoursBonus-discountPay;
	
}
//=====================================================================================================

void ShowResults(){
	cout<<"\r\n============Show result============\r\n";
	cout<<"The week pay is: ";
	cout<<"\n\t"<<payTotalWeek;
}

//=====================================================================================================
double CalculateGrossSalary(float workHours, float payHours){
	return workHours*payHours; 
}
//=====================================================================================================

double CalculatePayBonus(float workHours, float payHour, int hoursMax){
	return (workHours-hoursMax)*payHour*payForHoursExtraFactor; 
}
//=====================================================================================================

bool CheckHoursWork(float workHours){
	bool bonus=false;
	if (workHours>hoursMax){
		bonus=true;
	}
	return bonus;
}
//=====================================================================================================

bool CheckGrossSalary(double salary){
	bool discount=false;
	if (salary>salaryMax){
		discount=true;
	}
	return discount;
}
//=====================================================================================================
double CalculateDiscount(double grossSalary){
	return grossSalary*0.1;
}
