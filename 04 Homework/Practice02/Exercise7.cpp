/**
 * @file Exercise7.cpp
 * @author Alexis D�az Huaringa (alexis.diaz.h@uni.pe)
 * @brief exercise 7
 * @version 1.0
 * @date 31.01.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include "math.h"
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int distanceTraveledInput=0;
int fixedAmount=250;
int distanceA=300;
int distanceB=1000;
int firstFee=30;
int secondFee=20;
int payFeeDistance=0;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
int CalculatePayFeeDistance(int distanceTraveledInput,int firstFee, int secondFee);
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
*  												FUNCTION DEFINITION
*******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}
//=====================================================================================================

void CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Insert the travel distance: ";
	cin>>distanceTraveledInput;
}
//=====================================================================================================

void Calculate(){
	payFeeDistance+=fixedAmount;
	payFeeDistance=CalculatePayFeeDistance(distanceTraveledInput,firstFee,secondFee);
}
//=====================================================================================================

void ShowResults(){
	cout<<"\r\n============Show result============\r\n";
	cout<<"The rent payment is: ";
	cout<<"\n\t"<<payFeeDistance;
}
//=====================================================================================================

int CalculatePayFeeDistance(int distanceTraveledInput,int firstFee, int secondFee){
	if (distanceTraveledInput>distanceB){
		payFeeDistance+=secondFee*(distanceTraveledInput-distanceB)+(distanceB-distanceA)*firstFee;
	}else if (distanceTraveledInput>distanceA){
		payFeeDistance+=firstFee*(distanceTraveledInput-distanceA);
	}else{
		payFeeDistance+=0;
	}
	return payFeeDistance;
}
//=====================================================================================================
