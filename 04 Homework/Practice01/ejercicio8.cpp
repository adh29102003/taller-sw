/**
 * @file CodeReviewExercise08.cpp
 * @author Alexis D�az Huaringa (alexis.diaz.h@uni.pe)
 * @brief exercise 08
 * @version 1.0
 * @date 17.12.2021
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include "math.h"
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
double capitalInicialInput=0;
double montoFinal= 0;
double interesInput= 0;
double numeroPeriodosInput=0;
double numeroAnioInput=0;



/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
double calcularMontoFinal(double interesInput, int numeroPeriodosInput, int numeroAnioInput, int capitalInicialInput);
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
*  												FUNCTION DEFINITION
*******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}
//=====================================================================================================

void CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Ingrese: \nCapital inicial:  ";
	cin>>capitalInicialInput;
	cout<<"\nTipo de interes nominal:  ";
	cin>>interesInput;
	cout<<"\nNumero de a�os:  ";
	cin>>numeroAnioInput;
	cout<<"\nN�mero de periodos por a�o:  ";
	cin>>numeroPeriodosInput;
}
//=====================================================================================================

void Calculate(){
	montoFinal=calcularMontoFinal(interesInput, numeroPeriodosInput,numeroAnioInput,capitalInicialInput);
	
}
//=====================================================================================================

void ShowResults(){
	cout<<"\r\n============Show result============\r\n";
	cout<<"El monto final es:\n"<<montoFinal;
}
//=====================================================================================================

double calcularMontoFinal(double interesInput, int numeroPeriodosInput, int numeroAnioInput, int capitalInicialInput){
	double a=1+interesInput/(100*numeroPeriodosInput);
	double b= numeroAnioInput*numeroPeriodosInput;
	return capitalInicialInput*pow(a,b);
}
