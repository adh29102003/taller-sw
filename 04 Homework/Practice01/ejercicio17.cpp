/**
 * @file CodeReviewExercise17.cpp
 * @author Alexis D�az Huaringa (alexis.diaz.h@uni.pe)
 * @brief exercise 17
 * @version 1.0
 * @date 31.12.2021
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include "math.h"
#define PI 3.1415926
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float mayorRadioInput=0.0;
float menorRadioInput=0.0;
double areaElipse=0;
double perimetroElipse=0;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
double AreaElipse(float mayorRadio, float menorRadio);
double PerimetroElipse(float mayorRadio, float menorRadio);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
*  												FUNCTION DEFINITION
*******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}
//=====================================================================================================

void CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Ingrese el menor radio de la elipse: ";
	cin>>menorRadioInput;
	cout<<"Ingrese el mayor radio de la elipse: ";
	cin>>mayorRadioInput;
}
//=====================================================================================================

void Calculate(){
	areaElipse=AreaElipse(mayorRadioInput,menorRadioInput);
	perimetroElipse=PerimetroElipse(mayorRadioInput,menorRadioInput);
	
	
}
//=====================================================================================================

void ShowResults(){
	cout<<"\r\n============Show result============\r\n";
	cout<<"El area de la elipse es: ";
	cout<<"\n\t"<<areaElipse;
	cout<<"\nEl perimetro de la elipse es: ";
	cout<<"\n\t"<<perimetroElipse;
}
//=====================================================================================================

double AreaElipse(float mayorRadio, float menorRadio){
	double area=PI*mayorRadio*menorRadio;
	return area;
}
//=====================================================================================================

double PerimetroElipse(float mayorRadio,float menorRadio){
	double perimetro=PI*sqrt(4*(mayorRadio+menorRadio)/2);
	return perimetro;
}
