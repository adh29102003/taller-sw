/**
 * @file CodeReviewExercise11.cpp
 * @author Alexis D�az Huaringa (alexis.diaz.h@uni.pe)
 * @brief exercise 11
 * @version 1.0
 * @date 17.12.2021
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include "math.h"
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
double metrosMedidaInput=0;
double piesMedida= 0;
double yardasMedida= 0;
double centimetrosMedida=0;
double pulgadasMedida=0;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
double CentrimetrosConvercion(double metrosMedidaInput);
double PulgadasConvercion(double centimetrosMedida);
double PiesConvercion(double pulgadasMedida);
double YardasConvercion(double piesMedida);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
*  												FUNCTION DEFINITION
*******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}
//=====================================================================================================

void CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Ingrese la medida en metros: ";
	cin>>metrosMedidaInput;
}
//=====================================================================================================

void Calculate(){
	centimetrosMedida=CentrimetrosConvercion(metrosMedidaInput);
	pulgadasMedida=PulgadasConvercion(centimetrosMedida);
	piesMedida=PiesConvercion(pulgadasMedida);
	yardasMedida=YardasConvercion(piesMedida);
	
}
//=====================================================================================================

void ShowResults(){
	cout<<"\r\n============Show result============\r\n";
	cout<<"La medida convertida es:";
	cout<<"\n\tCentimetros: "<<centimetrosMedida;
	cout<<"\n\tPulgadas: "<<pulgadasMedida;
	cout<<"\n\tPies: "<<piesMedida;
	cout<<"\n\tYardas: "<<yardasMedida;
}
//=====================================================================================================

double CentrimetrosConvercion(double metrosMedidaInput){
	return metrosMedidaInput*100;
}
//=====================================================================================================

double PulgadasConvercion(double centimetrosMedida){
	return centimetrosMedida/2.54;
}
//=====================================================================================================

double PiesConvercion(double pulgadasMedida){
	return pulgadasMedida/12;
}
//=====================================================================================================

double YardasConvercion(double piesMedida){
	return piesMedida/3;
}
