/**
 * @file CodeReviewExercise3.cpp
 * @author Alexis D�az Huaringa (alexis.diaz.h@uni.pe)
 * @brief exercise 3
 * @version 1.0
 * @date 25.01.22
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>

using namespace std;
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/

 	int   totalEstudiantes = 0;					//Datos estadisticos
 	int   desaprobadosEstudiantesInput = 0;			//Entrada de los estudiantes desaprobados
 	int   aprobadosEstudiantesInput= 0;				//Entrada de los estudiantes aprobados
 	int   notablesEstudiantesInput= 0;				//Entrada de los estudiantes notables
 	int   sobresalientesEstudiantesInput= 0;			//Entrada de los estudiantes sobresalientes
 	float superaronPorcentaje= 0.0;			//Porcentaje de los que superaron el curso					
 	float desaprobadoPorcentaje= 0.0;		//Porcentaje de los que desaprobaron el curso
 	float aprobadoPorcentaje= 0.0;		//Porcentaje de los que aprobaron el curso
 	float notablesPorcentaje= 0.0;			//Porcentaje de los notables en el curso
 	float sobresalientesPorcentaje= 0.0;		//Porcentaje de los sobresalientes del curso

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
int CalcularTotalEstudiante(int desaprobado, int aprobado, int notable, int sobresaliente);
float CalcularEstudiantesPasaronCurso(int total, int aprobado, int notable, int sobresaliente);
float CalcularDesaprobadosPorcentajeCurso(int total, int desaprobado);
float CalcularAprobadosPorcentajeCurso(int total, int aprobado);
float CalcularNotablesPorcentajeCurso(int total, int notable);
float CalcularSobresalientesPorcentajeCurso(int total, int sobresaliente);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

 int main(){
	
	Run();
 	return 0;
 	
 }
 /*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

void CollectData(){	
 	cout<<"============Insert data============\r\n";
 	cout<<"Calculation in a course\r\n";
 	cout<<"\tIngrese el numero de desaprobados: ";
 	cin>>desaprobadosEstudiantesInput;
 	cout<<"\tIngrese el numero de estudiantes aprobados: ";
 	cin>>aprobadosEstudiantesInput;
 	cout<<"\tIngrese el numero de estudiantes notables: ";
 	cin>>notablesEstudiantesInput;
 	cout<<"\tIngrese el numero de estudiantes sobresalientes: ";
 	cin>>sobresalientesEstudiantesInput;
 	
}
//=====================================================================================================

void Calculate(){
	totalEstudiantes=CalcularTotalEstudiante(desaprobadosEstudiantesInput,aprobadosEstudiantesInput,notablesEstudiantesInput,sobresalientesEstudiantesInput);
	superaronPorcentaje=CalcularEstudiantesPasaronCurso(totalEstudiantes,aprobadosEstudiantesInput,notablesEstudiantesInput,sobresalientesEstudiantesInput);
	desaprobadoPorcentaje=CalcularDesaprobadosPorcentajeCurso(totalEstudiantes,desaprobadosEstudiantesInput);
	aprobadoPorcentaje=CalcularAprobadosPorcentajeCurso(totalEstudiantes,aprobadosEstudiantesInput);
	notablesPorcentaje=CalcularNotablesPorcentajeCurso(totalEstudiantes,notablesEstudiantesInput);
	sobresalientesPorcentaje=CalcularSobresalientesPorcentajeCurso(totalEstudiantes,sobresalientesEstudiantesInput);
	
}
//=====================================================================================================
	//Show the calculation of the terminal
void ShowResults(){
 	cout<<"============Show result============\r\n";
 	cout<<"El total de estudiantes es:"<<totalEstudiantes<<"\r\n";
 	cout<<"\tEl procentaje de quienes pasaron el curso: "<<superaronPorcentaje<<"%\r\n";
 	cout<<"\tEl porcentaje de quienes desaprobaron el curso: "<<desaprobadoPorcentaje<<"%\r\n";
 	cout<<"\tEl procentaje de quienes aprobaron el curso:"<<aprobadoPorcentaje<<"%\r\n";
 	cout<<"\tEl porcentaje de quienes son notables del curso: "<<notablesPorcentaje<<"%\r\n";
 	cout<<"\tEl porcentaje de quienes son sobresalientes del curso: "<<sobresalientesPorcentaje<<"%\r\n";
}
//=====================================================================================================

int CalcularTotalEstudiante(int desaprobado, int aprobado, int notable, int sobresaliente){
	return desaprobado+aprobado+notable+sobresaliente;    //total=aprobado+desaprobado+notable+sobresaliente
}
//=====================================================================================================

float CalcularEstudiantesPasaronCurso(int total, int aprobado, int notable, int sobresaliente){
	return ((float)aprobado+(float)notable+(float)sobresaliente)*100.0/total;
}
//=====================================================================================================

float CalcularDesaprobadosPorcentajeCurso(int total, int desaprobado){
	return ((float)desaprobado)*100.0/total;
}
//=====================================================================================================

float CalcularAprobadosPorcentajeCurso(int total, int aprobado){
	return ((float)aprobado)*100.0/total;
}
//=====================================================================================================

float CalcularNotablesPorcentajeCurso(int total, int notable){
	return ((float)notable)*100.0/total;
}
//=====================================================================================================

float CalcularSobresalientesPorcentajeCurso(int total, int sobresaliente){
	return ((float)sobresaliente)*100.0/total;
}
//=====================================================================================================
