/**
 * @file CodeReviewExercise4.cpp
 * @author Alexis D�az Huaringa (alexis.diaz.h@uni.pe)
 * @brief exercise 4
 * @version 1.0
 * @date 24.01.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include "math.h"
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float celsiusInput=0.0; //Entrada de los grados celsius desde la terminal 
float aguaCantidadPulgadasInput=0.0; //Entrada de la cantidad de agua en pulgadas
double fahrenheitCalcular=0; //Convertir los grados celsius a fahrenheit 
double aguaCantidadMilimetrosInput=0; //Convertir la cantidad de agua de pulgadas a milimetros

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
double ConvertirFahrenheitTemperatura(float celsius);
double ConvertirMilimetrosAgua(float pulgadas);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
*  												FUNCTION DEFINITION
*******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}
//=====================================================================================================

void CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Ingrese la temperatura en grados celsius: ";
	cin>>celsiusInput;
	cout<<"Ingrese la cantidad de agua en pulgadas: ";
	cin>>aguaCantidadPulgadasInput;
}
//=====================================================================================================

void Calculate(){
	fahrenheitCalcular=ConvertirFahrenheitTemperatura(celsiusInput);
	aguaCantidadMilimetrosInput=ConvertirMilimetrosAgua(aguaCantidadPulgadasInput);
	
}
//=====================================================================================================

void ShowResults(){
	cout<<"\r\n============Show result============\r\n";
	cout<<"La temperatura en fahrenheit es: ";
	cout<<"\n\t"<<fahrenheitCalcular;
	cout<<"\nLa cantidad de agua en milimetros es: ";
	cout<<"\n\t"<<aguaCantidadMilimetrosInput;
}
//=====================================================================================================

double ConvertirFahrenheitTemperatura(float celsius){
	return celsius*1.8+32;  //Fahrenheit=9/5celcius+32	
}
//=====================================================================================================

double ConvertirMilimetrosAgua(float pulgadas){
	return pow(25.5,3)*pulgadas; //aguaMil�metros=(25.5^3)*aguapulgadas   (volumen)
	
}

	
