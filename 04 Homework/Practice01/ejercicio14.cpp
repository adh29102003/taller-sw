/**
 * @file CodeReviewExercise14.cpp
 * @author Alexis D�az Huaringa (alexis.diaz.h@uni.pe)
 * @brief exercise 14
 * @version 1.0
 * @date 28.01.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include "math.h"
#define PI 3.1415926
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float pequenoDiametroInput=0.0; //Entrada del diametro de los extremos del tonel desde la terminal
float grandeDiametroInput=0.0; //Entrada del diametro del centro del tonel desde la terminal
float longitudTonelInput=0.0; //Entrada de la longitud del tonel desde la terminal
double alturaTonel=0; //Calcula la altura del tonel
double capacidadTonel=0; //Calcular la capidad del tonel

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
double CalcularAlturaTonel(float pequenoDiametro, float grandeDiametro);
double CalcularCapacidadTonel(double altura, float longitud);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
*  												FUNCTION DEFINITION
*******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}
//=====================================================================================================

void CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Ingrese el diametro en sus extremos del tonel: ";
	cin>>pequenoDiametroInput;
	cout<<"Ingrese el diametro del centro del tonel: ";
	cin>>grandeDiametroInput;
	cout<<"Ingrese la longitud del tonel: ";
	cin>>longitudTonelInput;
}
//=====================================================================================================

void Calculate(){
	alturaTonel=CalcularAlturaTonel(pequenoDiametroInput,grandeDiametroInput);
	capacidadTonel=CalcularCapacidadTonel(alturaTonel, longitudTonelInput);
	
}
//=====================================================================================================

void ShowResults(){
	cout<<"\r\n============Show result============\r\n";
	cout<<"La capacidad del tonel es: ";
	cout<<"\n\t"<<capacidadTonel<<"cm3"<<endl;
	cout<<"\n\t"<<capacidadTonel*0.001<<"L"<<endl;
	cout<<"\n\t"<<capacidadTonel/1000000<<"m3"<<endl;
}
//=====================================================================================================

double CalcularAlturaTonel(float pequenoDiametro, float grandeDiametro){
	return pequenoDiametro/2.0+2*(grandeDiametro/2-pequenoDiametro/2)/3.0;
}
//=====================================================================================================

double CalcularCapacidadTonel(double altura, float longitud){
	return PI*longitud*pow(altura,2);
}
