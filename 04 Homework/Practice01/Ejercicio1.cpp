/**
 * @file CodeReviewExercise1.cpp
 * @author Alexis D�az Huaringa (alexis.diaz.h@uni.pe)
 * @brief exercise 1
 * @version 1.0
 * @date 24.01.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include "math.h"
#define PI 3.1415926
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float radioCilindroInput=0.0; //Entrada del radio del cilindro a travez de la terminal 
float alturaCilindroInput=0.0; //Entrada de la altura del cilindro a travez de la terminal 
double areaCilindroLateral=0; //Calcular el area lateral del cilindro
double volumenCilindro=0; //Calcular el volumen del cilindro

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
double CalcularAreaLateralCilindro(float radio, float altura);
double CalcularVolumenCilindro(float radio, float altura);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
*  												FUNCTION DEFINITION
*******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}
//=====================================================================================================

void CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Ingrese el radio del cilindro: ";
	cin>>radioCilindroInput;
	cout<<"Ingrese la altura del cilindro: ";
	cin>>alturaCilindroInput;
}
//=====================================================================================================

void Calculate(){
	areaCilindroLateral=CalcularAreaLateralCilindro(radioCilindroInput,alturaCilindroInput);
	volumenCilindro=CalcularVolumenCilindro(radioCilindroInput,alturaCilindroInput);
	
}
//=====================================================================================================

void ShowResults(){
	cout<<"\r\n============Show result============\r\n";
	cout<<"El area lateral del cilindro es: ";
	cout<<"\n\t"<<areaCilindroLateral;
	cout<<"\nEl volumen del cilindro es: ";
	cout<<"\n\t"<<volumenCilindro;
}
//=====================================================================================================

double CalcularAreaLateralCilindro(float radio, float altura){
	return 2.0* PI * radio * altura; //area lateral=2*radio*pi*altura;
}
//=====================================================================================================

double CalcularVolumenCilindro(float radio, float altura){
	return pow(radio,2)*altura* PI; // volumen=(radio^2)*PI*altura
}
//=====================================================================================================

