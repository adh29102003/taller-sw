/**
 * @file CodeReviewExercise1.cpp
 * @author Alexis D�az Huaringa (alexis.diaz.h@uni.pe)
 * @brief exercise 1
 * @version 1.0
 * @date 24.01.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include "math.h"

using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float hombresCantidadInput=0.0; //Entrada de la cantidad de hombres en el grupo
float mujeresCantidadInput=0.0; //Entrada de la cantidad de mujeres en el grupo
double hombresPorcentaje=0; //Porcentaje de hombres en el grupo
double mujeresPorcentaje=0; //Porcentaje de mujeres en el grupo
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
double CalcularPorcentaje(float numero);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
*  												FUNCTION DEFINITION
*******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}
//=====================================================================================================

void CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Ingrese la cantidad de mujeres en el grupo: ";
	cin>>mujeresCantidadInput;
	cout<<"Ingrese la cantidad de hombres en el grupo: ";
	cin>>hombresCantidadInput;
}
//=====================================================================================================

void Calculate(){
	hombresPorcentaje=CalcularPorcentaje(hombresCantidadInput);
	mujeresPorcentaje=CalcularPorcentaje(mujeresCantidadInput);
	
}
//=====================================================================================================

void ShowResults(){
	cout<<"\r\n============Show result============\r\n";
	cout<<"El porcentaje de mujeres en el grupo es: ";
	cout<<"\n\t"<<mujeresPorcentaje<<"%";
	cout<<"\nEl porcentaje de hombres en el grupo es: ";
	cout<<"\n\t"<<hombresPorcentaje<<"%";
}
//=====================================================================================================

double CalcularPorcentaje(float numero){
	double total=hombresCantidadInput+mujeresCantidadInput;
	return numero*100/total; //Regresa el porcentaje de la cantidad requerida
}
//=====================================================================================================
