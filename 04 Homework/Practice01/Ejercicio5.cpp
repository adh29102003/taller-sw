/**
 * @file CodeReviewExercise1.cpp
 * @author Alexis D�az Huaringa (alexis.diaz.h@uni.pe)
 * @brief exercise 1
 * @version 1.0
 * @date 24.01.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include "math.h"
#define PI 3.1415926
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float costoVendedorVehiculoInput=0.0; //Entrada del costo que paga el vendedor por el vehiculo
double gananciaVendedor=0; //Calcular la ganancia del vendedor
double impuestosLocalesVehiculo=0; //Calcular los impuestos de la venta del vehiculo
double costoConsumidorVehiculo=0; //Calcular el costo que pagan los compradores 

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
double CalcularCostoConsumidorVehiculo(float costoVendedorVehiculoInput);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
*  												FUNCTION DEFINITION
*******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}
//=====================================================================================================

void CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Ingrese el costo del vehiculo que paga el vendedor: ";
	cin>>costoVendedorVehiculoInput;
}
//=====================================================================================================

void Calculate(){
	costoConsumidorVehiculo=CalcularCostoConsumidorVehiculo(costoVendedorVehiculoInput);
	
}
//=====================================================================================================

void ShowResults(){
	cout<<"\r\n============Show result============\r\n";
	cout<<"El precio que paga el consumidor es: ";
	cout<<"\n\t"<<costoConsumidorVehiculo;
}
//=====================================================================================================

double CalcularCostoConsumidorVehiculo(float costoVendedorVehiculoInput){
	gananciaVendedor=0.12*costoVendedorVehiculoInput;
	impuestosLocalesVehiculo=0.06*costoVendedorVehiculoInput;
	return costoVendedorVehiculoInput+gananciaVendedor+impuestosLocalesVehiculo; //el costoConsumidor=ganancia+impuesto+costoVendedor
}
	
